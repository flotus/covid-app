import { Component } from '@angular/core';
import { CovidService } from '../covid.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  summary: any[] = [];
  lastDate: null;
  n_paises;

  constructor( public servicio: CovidService){}
  ngOnInit(){
    this.servicio.getPage()
    .subscribe(
      (data) =>{
        this.summary = data['Global'];
        this.lastDate = data['Date'];
        this.n_paises = this.getNewData(data['Countries'], this.servicio.getSelectedCountries());
        console.log('ok');
      }, (error) =>{
        console.error(error);
      }
    )
  }
  getNewData(countries, selected){
    let Data_n = [];
    for (let i = 0; i <selected.length;i++){
    
      for(let j = 0; i<countries.length;j++){
        
        if (selected[i]['ISO2'] == countries[j]['CountryCode']){
          
          
          Data_n.push(countries[j]);
          break;
        }
      }
    }
    return Data_n;
  }
  removeCountry(countryCode){
    let data = [];
    this.servicio.getSelectedCountries().forEach(element => {
      if(element['ISO2'] != countryCode){
        data.push(element);
      }
      
    });
    this.servicio.setSelectedCountries(data);
  }

}

