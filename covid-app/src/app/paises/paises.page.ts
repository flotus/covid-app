import { Component, OnInit } from '@angular/core';
import { CovidService } from '../covid.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-paises',
  templateUrl: './paises.page.html',
  styleUrls: ['./paises.page.scss'],
})
export class PaisesPage implements OnInit {

  paises: any;
  selected: any;

  constructor( public service: CovidService, public router: Router) {
  }
  ngOnInit(){
    this.service.getCountries().subscribe(
      (data) => {
      this.paises = data;
      this.paises.sort((a,b) => a.Country.toLowerCase() > b.Country.toLowerCase()? 1: -1);
      this.selected.forEach(elemento => {
        if (elemento['isChecked']){
          this.paises.forEach(element => {
            if (element['ISO2'] == elemento['ISO2']){
              element['isChecked'] = true;
              console.log(element);
            }
          });
        }
        
      });
      },
      (error) => {
        console.error(error);
      }
      )
      this.selected = this.service.getSelectedCountries();
      

      
    
  }
  onSubmit(){
    let data = [];
 
    /*for (let i = 0; i<this.countries.length;i++){
      console.log('itere');
      if (this.countries[i][])
    }*/
    this.paises.forEach(element => {
      if (element.isChecked){
        data.push(element);
      }
      
    });
    console.log(data);
    this.router.navigate(['/home']);
    this.service.setSelectedCountries(data);
    
  }
}
